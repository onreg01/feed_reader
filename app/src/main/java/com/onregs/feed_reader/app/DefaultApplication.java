package com.onregs.feed_reader.app;

import android.app.Application;

import com.onregs.feed_reader.BuildConfig;
import com.parse.Parse;

/**
 * Created by vadim on 23.12.2015.
 */
public class DefaultApplication extends Application {

    private static DefaultApplication INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;

        Parse.enableLocalDatastore(this);
        Parse.initialize(this);
        String s = BuildConfig.FLAVOR;
    }

    public static Application getApp()
    {
        return INSTANCE;
    }
}

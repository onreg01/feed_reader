package com.onregs.feed_reader.utils;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

public abstract class SnackbarUtils {

    public static Snackbar makeSnackbar(View anchor, CharSequence text, int duration, int textColor)
    {
        Snackbar snackbar =  Snackbar.make(anchor, text, duration);
        ((TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text)).setTextColor(textColor);
        return snackbar;
    }
}

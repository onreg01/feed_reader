package com.onregs.feed_reader.listener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;

import com.onregs.feed_reader.R;
import com.onregs.feed_reader.data.Feed;
import com.onregs.feed_reader.ui.activity.DetailViewActivity;
import com.onregs.feed_reader.ui.adapter.FeedListCursorAdapter;

/**
 * Created by vadim on 24.12.2015.
 */
public class ListItemClickListener implements FeedListCursorAdapter.OnItemClickListener {

    private Context mContext;

    public ListItemClickListener(Context context){
        this.mContext = context;
    }

    @Override
    public void onItemClick(Cursor cursor) {
        if (cursor.getString(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_FEED_TYPE)).equals(Feed.TYPE_EXTERNAL)) {
            Uri uri = Uri.parse(cursor.getString(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_FEED_LINK)));
            showChromeActivity(uri);
        } else {
            Intent intent = new Intent(mContext, DetailViewActivity.class);
            intent.putExtra(Feed.FeedEntry._ID, cursor.getString(cursor.getColumnIndex(Feed.FeedEntry._ID)));
            mContext.startActivity(intent);
        }
    }

    public void showChromeActivity(Uri uri) {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(mContext.getResources().getColor(R.color.primary));

        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl((Activity) mContext, uri);
    }
}

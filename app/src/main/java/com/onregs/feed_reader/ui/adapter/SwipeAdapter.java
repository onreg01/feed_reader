package com.onregs.feed_reader.ui.adapter;

import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.onregs.feed_reader.R;

import java.util.ArrayList;
import java.util.LinkedList;

public class SwipeAdapter extends RecyclerView.Adapter<SwipeAdapter.MyViewHolder> {

    ArrayList<String> dataset;

    public SwipeAdapter(ArrayList<String> dataset) {
        this.dataset = dataset;
    }

    private ArrayList<Integer> changes = new ArrayList<>();

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.swipe_recylceer_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(dataset.get(position), position);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTextView1;
        private FrameLayout container;


        public MyViewHolder(View v) {
            super(v);
            mTextView1 = (TextView) itemView.findViewById(R.id.textView1);
            container = (FrameLayout) itemView.findViewById(R.id.container);
            v.findViewById(R.id.swipe_delete).setOnClickListener(this);
            v.findViewById(R.id.swipe_cancel).setOnClickListener(this);
        }

        public void bind(String text, int position) {
            if (changes.contains(position)) {
                container.setVisibility(View.VISIBLE);
                mTextView1.setVisibility(View.GONE);
            } else {
                mTextView1.setText(text);
                mTextView1.setVisibility(View.VISIBLE);
                container.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.swipe_cancel:
                    Integer pos = getAdapterPosition();
                    changes.remove(pos);
                    notifyItemChanged(pos);
                    break;
                case R.id.swipe_delete:
                    changes.remove((Integer) getAdapterPosition());
                    dataset.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    break;
            }
        }
    }

    public boolean setChanges(Integer position) {
        if (changes.contains(position)) {
            changes.remove(position);
            return true;
        }
        changes.add(position);
        return false;
    }
}
package com.onregs.feed_reader.ui.activity;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.onregs.feed_reader.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        setToolbar();
        getFragmentManager().beginTransaction().replace(R.id.preference_container,
                new SettingsFragment()).commit();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsActivity.super.onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().popBackStackImmediate()) {
            getFragmentManager().popBackStack();
        } else {
            SettingsActivity.super.onBackPressed();
        }
    }

    public static class SettingsFragment extends PreferenceFragment {

        public static final String KEY_NOTIFICATIONS_SCREEN = "pref_notifications_screen";


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
            Preference preference = findPreference(KEY_NOTIFICATIONS_SCREEN);
            preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    getActivity().getFragmentManager().beginTransaction().replace(R.id.preference_container, new SettingsNotificationsFragment()).
                            addToBackStack(SettingsNotificationsFragment.class.getSimpleName()).commit();
                    return true;
                }
            });
        }
    }

    public static class SettingsNotificationsFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings_notifications);
        }
    }
}

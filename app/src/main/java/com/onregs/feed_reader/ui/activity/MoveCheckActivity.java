package com.onregs.feed_reader.ui.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.onregs.feed_reader.R;
import com.onregs.feed_reader.service.MoveService;
import com.onregs.feed_reader.utils.SnackbarUtils;

public class MoveCheckActivity extends AppCompatActivity {

    private MoveService service;
    private boolean mBound = false;
    private BroadcastReceiver receiver;
    private GoogleMap googleMap;
    private CoordinatorLayout coordinator;

    private Marker markerCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_check);
        coordinator = (CoordinatorLayout) findViewById(R.id.coordinator);
        ((MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment)).getMapAsync(mapReadyCallbacks);
        setToolbar();
        setReceiver();
        startService(new Intent(this, MoveService.class));
    }


    private OnMapReadyCallback mapReadyCallbacks = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            if (MoveCheckActivity.this.googleMap == null && googleMap != null) {
                MoveCheckActivity.this.googleMap = googleMap;
            }
        }
    };


    private void setReceiver() {
        receiver = new LocationReceiver(callbacks);
        IntentFilter filter = new IntentFilter(LocationReceiver.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoveCheckActivity.super.onBackPressed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MoveService.class);
        bindService(intent, serviceConnection, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(serviceConnection);
            mBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        stopService(new Intent(this, MoveService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_move_checker, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.distance:
                SnackbarUtils.makeSnackbar(coordinator, getString(R.string.distance_current,
                        Math.round(service.getDistance())), Snackbar.LENGTH_LONG, Color.WHITE).setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SnackbarUtils.makeSnackbar(coordinator, "Undo clicked", Snackbar.LENGTH_SHORT, Color.WHITE).show();
                    }
                }).setActionTextColor(Color.RED).show();
                return true;
            case R.id.distance_reset:
                if (googleMap != null && markerCurrent != null) {
                    googleMap.addMarker(new MarkerOptions().title(getString(R.string.reset_location_text))
                            .position(markerCurrent.getPosition()));
                    markerCurrent.remove();

                    SnackbarUtils.makeSnackbar(coordinator, getString(R.string.distance_reset_done),
                            Snackbar.LENGTH_LONG, Color.WHITE).show();
                    service.resetDistance();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            MoveService.MoveServiceBinder binder = (MoveService.MoveServiceBinder) iBinder;
            service = binder.getMoveService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    LocationReceiver.Callbacks callbacks = new LocationReceiver.Callbacks() {

        @Override
        public void updateLocation(double latitude, double longitude) {
            if (googleMap != null) {
                if(markerCurrent!=null) {
                    markerCurrent.remove();
                }
                MarkerOptions options = new MarkerOptions();
                options.position(new LatLng(latitude, longitude));
                options.title(getString(R.string.map_maprker_title));
                markerCurrent = googleMap.addMarker(options);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.5f));
            }
        }
    };

    public static class LocationReceiver extends BroadcastReceiver {
        public final static String BROADCAST_ACTION = "location.listener";
        public final static String LATITUDE = "latitude";
        public final static String LONGITUDE = "longitude";

        private Callbacks callbacks;

        public LocationReceiver(Callbacks callbacks) {
            this.callbacks = callbacks;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            callbacks.updateLocation(intent.getDoubleExtra(LATITUDE, 0), intent.getDoubleExtra(LONGITUDE, 0));
        }

        interface Callbacks {
            void updateLocation(double latitude, double longitude);
        }
    }

}

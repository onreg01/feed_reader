package com.onregs.feed_reader.ui.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.onregs.feed_reader.ui.fragment.FeedFragment;
import com.onregs.feed_reader.ui.fragment.FeedHotFragment;
import com.onregs.feed_reader.ui.fragment.FeedNewFragment;
import com.onregs.feed_reader.ui.fragment.FeedTopFragment;


/**
 * Created by vadim on 22.11.2015.
 */
public class FeedPageAdapter extends FragmentStatePagerAdapter {

    public static int PAGE_COUNT = 3;

    public FeedPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        FeedFragment fragment;

        switch (position) {
            case 0:
                fragment = new FeedHotFragment();
                break;
            case 1:
                fragment = new FeedNewFragment();
                break;
            case 2:
                fragment = new FeedTopFragment();
                break;
            default:
                fragment = new FeedHotFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


}
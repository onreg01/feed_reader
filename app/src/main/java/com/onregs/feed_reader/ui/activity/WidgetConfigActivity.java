package com.onregs.feed_reader.ui.activity;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.onregs.feed_reader.R;
import com.onregs.feed_reader.ui.widget.CustomWidgetProwider;

public class WidgetConfigActivity extends AppCompatActivity {

    int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
    Intent resultValue;

    final String LOG_TAG = "myLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "onCreate config");

        setToolbar();

        // извлекаем ID конфигурируемого виджета
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            widgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        // и проверяем его корректность
        if (widgetID == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        // формируем intent ответа
        resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);

        // отрицательный ответ
//        setResult(RESULT_CANCELED, resultValue);

        setContentView(R.layout.widget_config);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    public void onClick(View v) {
        int selRBColor = ((RadioGroup) findViewById(R.id.rgColor)).getCheckedRadioButtonId();

        int color = Color.RED;
        switch (selRBColor) {
            case R.id.radioRed:
                color = R.color.red;
                break;
            case R.id.radioGreen:
                color = R.color.green;
                break;
            case R.id.radioBlue:
                color = R.color.blue;
                break;
            case R.id.radioDefault:
                color = R.drawable.widget_bg;
                break;
        }
        EditText etText = (EditText) findViewById(R.id.etText);


        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        CustomWidgetProwider.updateWidget(this, appWidgetManager, widgetID, color, etText.getText().toString());

        // положительный ответ
        setResult(RESULT_OK, resultValue);

        Log.d(LOG_TAG, "finish config " + widgetID);
        finish();
    }

}

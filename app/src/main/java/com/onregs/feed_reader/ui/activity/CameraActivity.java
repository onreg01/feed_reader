package com.onregs.feed_reader.ui.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.onregs.feed_reader.R;
import com.onregs.feed_reader.ui.view.DrawingView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CameraActivity extends AppCompatActivity {

    SurfaceView sv;
    SurfaceHolder holder;
    HolderCallback holderCallback;
    Camera camera;
    DrawingView drawingView;
    boolean drawingViewSet;

    File photoFile;
    File videoFile;
    private CameraOrientationListener orientationListener;

    MediaRecorder mediaRecorder;
    private int displayOrientation;

    private FrameLayout container;


    final int CAMERA_ID = 0;
    final boolean FULL_SCREEN = true;

    private boolean isPreviewPhotoShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        sv = (SurfaceView) findViewById(R.id.surfaceView);

        container = (FrameLayout) findViewById(R.id.container);
        container.setOnTouchListener(touchListener);

        holder = sv.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        holderCallback = new HolderCallback();
        holder.addCallback(holderCallback);

        File pictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        photoFile = new File(pictures, "myphoto.jpg");
        videoFile = new File(pictures, "myvideo.mp4");

        findViewById(R.id.btnPhoto).setOnClickListener(onClickPhoto);
        findViewById(R.id.btnVideoStart).setOnClickListener(onClickVideoStart);
        findViewById(R.id.btnVideoStop).setOnClickListener(onClickVideoStop);

        orientationListener = new CameraOrientationListener(this);

        drawingView = (DrawingView) findViewById(R.id.drawing_surface);
        setDrawingView(drawingView);
    }

    View.OnClickListener onClickPhoto = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            if (camera.getParameters().getFocusMode().equals(Camera.Parameters.FOCUS_MODE_AUTO)){
                Log.v("MyLogs", "Auto focus on");
                camera.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {
                        if(success)
                        {
                            takePicture();
                        }
                    }
                });
            }else{
                Log.v("MyLogs", "Auto focus off");
                takePicture();
            }
        }
    };


    private void takePicture()
    {
        orientationListener.rememberOrientation();
        camera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                isPreviewPhotoShow = true;

                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

                int rotation = (displayOrientation + orientationListener.getRememberedOrientation()) % 360;

                if (rotation != 0) {
                    Bitmap oldBitmap = bitmap;

                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);

                    bitmap = Bitmap.createBitmap(
                            bitmap,
                            0,
                            0,
                            bitmap.getWidth(),
                            bitmap.getHeight(),
                            matrix,
                            false
                    );

                    oldBitmap.recycle();
                }

                try {
                    FileOutputStream fileStream = new FileOutputStream(photoFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileStream);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isPreviewPhotoShow) {
            camera.startPreview();
            isPreviewPhotoShow = false;
        } else {
            super.onBackPressed();
        }
    }

    View.OnClickListener onClickVideoStart = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            orientationListener.rememberOrientation();
            if (prepareVideoRecorder()) {
                mediaRecorder.start();
            } else {
                releaseMediaRecorder();
            }
        }
    };

    View.OnClickListener onClickVideoStop = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mediaRecorder != null) {
                mediaRecorder.stop();
                releaseMediaRecorder();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        orientationListener.enable();
        camera = Camera.open(CAMERA_ID);
//        setPreviewSize(FULL_SCREEN);
        setPreviewSize();
        initSpinners();
    }

    @Override
    public void onPause() {
        super.onPause();
        orientationListener.disable();
        if (camera != null)
            camera.release();
        camera = null;
    }

    class HolderCallback implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {
            camera.stopPreview();
            setCameraDisplayOrientation(CAMERA_ID);
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }
    }

//    void setPreviewSize(boolean fullScreen) {
//
//        // получаем размеры экрана
//        Display display = getWindowManager().getDefaultDisplay();
//        boolean widthIsMax = display.getWidth() > display.getHeight();
//
//        // определяем размеры превью камеры
//        Camera.Size size = camera.getParameters().getPreviewSize();
//
//        RectF rectDisplay = new RectF();
//        RectF rectPreview = new RectF();
//
//        // RectF экрана, соотвествует размерам экрана
//        rectDisplay.set(0, 0, display.getWidth(), display.getHeight());
//
//        // RectF первью
//        if (widthIsMax) {
//            // превью в горизонтальной ориентации
//            rectPreview.set(0, 0, size.width, size.height);
//        } else {
//            // превью в вертикальной ориентации
//            rectPreview.set(0, 0, size.height, size.width);
//        }
//
//        Matrix matrix = new Matrix();
//        // подготовка матрицы преобразования
//        if (!fullScreen) {
//            // если превью будет "втиснут" в экран (второй вариант из урока)
//            matrix.setRectToRect(rectPreview, rectDisplay,
//                    Matrix.ScaleToFit.START);
//        } else {
//            // если экран будет "втиснут" в превью (третий вариант из урока)
//            matrix.setRectToRect(rectDisplay, rectPreview,
//                    Matrix.ScaleToFit.START);
//            matrix.invert(matrix);
//        }
//        // преобразование
//        matrix.mapRect(rectPreview);
//
//        // установка размеров surface из получившегося преобразования
//        sv.getLayoutParams().height = (int) (rectPreview.bottom);
//        sv.getLayoutParams().width = (int) (rectPreview.right);
//    }

    private void setPreviewSize()
    {
        Display display = getWindowManager().getDefaultDisplay();
        Point sizepoint = new Point();
        display.getSize(sizepoint);
        int width = sizepoint.x;
        int height = sizepoint.y;

        Camera.Parameters p = camera.getParameters();
        List<Camera.Size> size1 = p.getSupportedPreviewSizes();
        Camera.Size size_to_use  = getOptimalSize(size1, width, height);


        p.setPreviewSize(size_to_use.width, size_to_use.height);
        camera.setParameters(p);
    }

    private Camera.Size getOptimalSize(List<Camera.Size> sizes, int w, int h) {

        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null)
            return null;
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;

        for (Camera.Size size : sizes)
        {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff)
            {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        // Cannot find the one match the aspect ratio, ignore the requirement

        if (optimalSize == null)
        {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff)
                {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    void setCameraDisplayOrientation(int cameraId) {
        // определяем насколько повернут экран от нормального положения
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result = 0;
        // получаем инфо по камере cameraId
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        displayOrientation = result % 360;
        camera.setDisplayOrientation(displayOrientation);
    }

    private boolean prepareVideoRecorder() {

        camera.unlock();

        mediaRecorder = new MediaRecorder();

        mediaRecorder.setCamera(camera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        mediaRecorder.setOutputFile(videoFile.getAbsolutePath());

        int rotation = (displayOrientation + orientationListener.getRememberedOrientation()) % 360;
        mediaRecorder.setOrientationHint(rotation);
//        mediaRecorder.setPreviewDisplay(sv.getHolder().getSurface());

        try {
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            camera.lock();
        }
    }

    public static class CameraOrientationListener extends OrientationEventListener {
        private int currentNormalizedOrientation;
        private int rememberedNormalizedOrientation;

        public CameraOrientationListener(Context context) {
            super(context, SensorManager.SENSOR_DELAY_NORMAL);
        }

        @Override
        public void onOrientationChanged(int orientation) {
            if (orientation != ORIENTATION_UNKNOWN) {
                currentNormalizedOrientation = normalize(orientation);
            }
        }

        private int normalize(int degrees) {
            if (degrees > 315 || degrees <= 45) {
                return 0;
            }

            if (degrees > 45 && degrees <= 135) {
                return 90;
            }

            if (degrees > 135 && degrees <= 225) {
                return 180;
            }

            if (degrees > 225 && degrees <= 315) {
                return 270;
            }

            throw new RuntimeException("The physics as we know them are no more. Watch out for anomalies.");
        }

        public void rememberOrientation() {
            rememberedNormalizedOrientation = currentNormalizedOrientation;
        }

        public int getRememberedOrientation() {
            return rememberedNormalizedOrientation;
        }
    }

    void initSpinners() {
        // Цветовые эффекты
        // получаем список цветовых эффектов
        final List<String> colorEffects = camera.getParameters().getSupportedColorEffects();
        Spinner spEffect = initSpinner(R.id.camera_spinner1, colorEffects, camera.getParameters().getColorEffect());
        // обработчик выбора
        spEffect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Camera.Parameters params = camera.getParameters();
                params.setColorEffect(colorEffects.get(arg2));
                camera.setParameters(params);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        // Режимы вспышки
        // получаем список режимов вспышки
        final List<String> flashModes = camera.getParameters().getSupportedFlashModes();
        // настройка спиннера
        Spinner spFlash = initSpinner(R.id.camera_spinner2, flashModes, camera.getParameters().getFlashMode());
        // обработчик выбора
        spFlash.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                Camera.Parameters params = camera.getParameters();
                params.setFlashMode(flashModes.get(arg2));
                camera.setParameters(params);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        // Режимы сцен
        // получаем список режимов сцены
        final List<String> sceneModes = camera.getParameters().getSupportedSceneModes();
        // настройка спиннера
        Spinner scene = initSpinner(R.id.camera_spinner3, sceneModes, camera.getParameters().getSceneMode());
        // обработчик выбора
        scene.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Camera.Parameters params = camera.getParameters();
                params.setSceneMode(sceneModes.get(arg2));
                camera.setParameters(params);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        // Режимы Anti-Banding
        // получаем список режимов Anti-Banding
        final List<String> antiBanding = camera.getParameters().getSupportedAntibanding();
        // настройка спиннера
        Spinner banding = initSpinner(R.id.camera_spinner4, antiBanding, camera.getParameters().getAntibanding());
        // обработчик выбора
        banding.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Camera.Parameters params = camera.getParameters();
                params.setAntibanding(antiBanding.get(arg2));
                camera.setParameters(params);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        // Режимы фокусы
        // получаем список фокусов
        final List<String> focus = camera.getParameters().getSupportedFocusModes();
        // настройка спиннера
        Spinner spinnerFoucs = initSpinner(R.id.camera_spinner5, focus, camera.getParameters().getFocusMode());
        // обработчик выбора
        spinnerFoucs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Camera.Parameters params = camera.getParameters();
                params.setFocusMode(focus.get(arg2));
                camera.setParameters(params);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        // Zoom
        int maxZoom = camera.getParameters().getMaxZoom();
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBarZoom);
        seekBar.setMax(maxZoom);
        seekBar.setProgress(camera.getParameters().getZoom());
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Camera.Parameters params = camera.getParameters();
                params.setZoom(progress);
                camera.setParameters(params);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


//        //Metering areas
//        Camera.Parameters params = camera.getParameters();
//
//        if (params.getMaxNumMeteringAreas() > 0){ // check that metering areas are supported
//            List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
//
//            Rect areaRect1 = new Rect(-100, -100, 100, 100);    // specify an area in center of image
//            meteringAreas.add(new Camera.Area(areaRect1, 600)); // set weight to 60%
//            Rect areaRect2 = new Rect(800, -1000, 1000, -800);  // specify an area in upper right of image
//            meteringAreas.add(new Camera.Area(areaRect2, 400)); // set weight to 40%
//            params.setMeteringAreas(meteringAreas);
//        }
//
//        camera.setParameters(params);
    }

    Spinner initSpinner(int spinnerId, List<String> data, String currentValue) {
        // настройка спиннера и адаптера для него
        Spinner spinner = (Spinner) findViewById(spinnerId);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        // определеяем какое значение в списке является текущей настройкой
        for (int i = 0; i < data.size(); i++) {
            String item = data.get(i);
            if (item.equals(currentValue)) {
                spinner.setSelection(i);
            }
        }

        return spinner;
    }


    Camera.AutoFocusCallback myAutoFocusCallback = new Camera.AutoFocusCallback(){

        @Override
        public void onAutoFocus(boolean arg0, Camera arg1) {
            if (arg0){
                Log.v("MyLogs", "Touch focus done");
                drawingView.setColor(Color.GREEN);
                drawingView.invalidate();
            }else
            {
                drawingView.setColor(Color.RED);
                drawingView.invalidate();
            }

            // Remove the square indicator after 1000 msec
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    drawingView.setHaveTouch(false, new Rect(0,0,0,0));
                    drawingView.invalidate();
                }
            }, 1000);
        }
    };

    /**
     * Called from PreviewSurfaceView to set touch focus.
     * @param - Rect - new area for auto focus
     */
    public void doTouchFocus(final Rect tfocusRect) {
        try {
            List<Camera.Area> focusList = new ArrayList<Camera.Area>();
            Camera.Area focusArea = new Camera.Area(tfocusRect, 1000);
            focusList.add(focusArea);

            Camera.Parameters param = camera.getParameters();
            param.setFocusAreas(focusList);
            param.setMeteringAreas(focusList);
            camera.setParameters(param);

            camera.autoFocus(myAutoFocusCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    View.OnTouchListener touchListener = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                float x = event.getX();
                float y = event.getY();

                Rect touchRect = new Rect(
                        (int)(x - 50),
                        (int)(y - 50),
                        (int)(x + 50),
                        (int)(y + 50));


                int left = touchRect.left * 2000/container.getWidth() - 1000;
                int top = touchRect.top * 2000/container.getHeight() - 1000;
                int right = touchRect.right * 2000/container.getWidth() - 1000;
                int bottom = touchRect.bottom * 2000/container.getHeight() - 1000;

                final Rect targetFocusRect = new Rect(
                        left,
                        top,
                        right,
                        bottom);

                doTouchFocus(targetFocusRect);
                if (drawingViewSet) {
                    drawingView.setHaveTouch(true, touchRect).setColor(Color.WHITE);
                    drawingView.invalidate();
                }
            }

            return false;
        }
    };


    public void setDrawingView(DrawingView dView) {
        drawingView = dView;
        drawingViewSet = true;
    }
}
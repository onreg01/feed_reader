package com.onregs.feed_reader.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;

import com.onregs.feed_reader.R;
import com.onregs.feed_reader.ui.view.DrawLineView;

import java.util.LinkedList;

public class DrawActivity extends AppCompatActivity implements View.OnTouchListener {

    private DrawLineView drawView;

    private LinkedList<XYCoordinates> coordinates = new LinkedList<>();
    private XYCoordinates xyCoordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw);
        drawView = (DrawLineView) findViewById(R.id.drawing_view);
        drawView.setOnTouchListener(this);
        drawView.invalidate();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xyCoordinates = new XYCoordinates();
                xyCoordinates.setX1(event.getX());
                xyCoordinates.setY1(event.getY());
                break;
            case MotionEvent.ACTION_MOVE:
                xyCoordinates.setX2(event.getX());
                xyCoordinates.setY2(event.getY());
                drawView.setXYCoordinates(xyCoordinates);
                drawView.invalidate();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                xyCoordinates.setX2(event.getX());
                xyCoordinates.setY2(event.getY());
                coordinates.add(xyCoordinates);
                drawView.setXYCoordinates(coordinates);
                drawView.invalidate();
                break;
        }

        return true;
    }

    public static class XYCoordinates {
        private float x1;
        private float y1;

        private float x2;
        private float y2;

        public float getX1() {
            return x1;
        }

        public void setX1(float x1) {
            this.x1 = x1;
        }

        public float getY1() {
            return y1;
        }

        public void setY1(float y1) {
            this.y1 = y1;
        }

        public float getX2() {
            return x2;
        }

        public void setX2(float x2) {
            this.x2 = x2;
        }

        public float getY2() {
            return y2;
        }

        public void setY2(float y2) {
            this.y2 = y2;
        }
    }

}

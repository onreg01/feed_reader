package com.onregs.feed_reader.ui.widget;

import java.util.Arrays;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.util.Log;
import android.widget.RemoteViews;

import com.onregs.feed_reader.R;

public class CustomWidgetProwider extends AppWidgetProvider {

    final static String LOG_TAG = "myLogs";

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d(LOG_TAG, "onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Log.d(LOG_TAG, "onUpdate " + Arrays.toString(appWidgetIds));
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        Log.d(LOG_TAG, "onDeleted " + Arrays.toString(appWidgetIds));
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.d(LOG_TAG, "onDisabled");
    }

    public static void updateWidget(Context context, AppWidgetManager appWidgetManager,int widgetID, int widgetColor, String widgetText) {
        Log.d(LOG_TAG, "updateWidget " + widgetID);

        if (widgetText == null) return;

        RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.widget_custom);

        widgetView.setInt(R.id.widget_layout, "setBackgroundResource", widgetColor);
        widgetView.setTextViewText(R.id.widget_text, widgetText);

        appWidgetManager.updateAppWidget(widgetID, widgetView);
    }
}
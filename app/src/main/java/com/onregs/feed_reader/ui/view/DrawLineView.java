package com.onregs.feed_reader.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.onregs.feed_reader.ui.activity.DrawActivity.XYCoordinates;

import android.view.View;

import com.onregs.feed_reader.ui.activity.DrawActivity;

import java.util.LinkedList;

public class DrawLineView extends View {

    private Paint paint;
    private LinkedList<XYCoordinates> coordinates;
    private XYCoordinates xyCoordinates;

    public DrawLineView(Context context) {
        super(context);
        init();

    }

    public DrawLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
    }

    public void setXYCoordinates(XYCoordinates xyCoordinates) {
        this.xyCoordinates = xyCoordinates;
    }

    public void setXYCoordinates(LinkedList<XYCoordinates> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.GREEN);
        if (coordinates != null) {
            for (XYCoordinates coordinate : coordinates) {
                canvas.drawLine(coordinate.getX1(), coordinate.getY1(), coordinate.getX2(), coordinate.getY2(), paint);
            }
        }
        if (xyCoordinates != null) {
            canvas.drawLine(xyCoordinates.getX1(), xyCoordinates.getY1(), xyCoordinates.getX2(), xyCoordinates.getY2(), paint);
        }
    }
}

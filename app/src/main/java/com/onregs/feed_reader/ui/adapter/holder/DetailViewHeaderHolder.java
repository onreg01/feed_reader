package com.onregs.feed_reader.ui.adapter.holder;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.onregs.feed_reader.R;
import com.onregs.feed_reader.data.Feed;
import com.onregs.feed_reader.data.MediaContent;
import com.onregs.feed_reader.data.db.content_provider.DataContentProvider;
import com.onregs.feed_reader.data.db.content_provider.async_query_handler.CustomAsyncQueryHandler;
import com.onregs.feed_reader.data.loader.ImageLoader;
import com.onregs.feed_reader.ui.adapter.GalleryAdapter;
import com.onregs.feed_reader.ui.adapter.decorator.GalleryDivider;
import com.onregs.feed_reader.ui.view.CustomImage;

/**
 * Created by vadim on 15.12.2015.
 */
public class DetailViewHeaderHolder extends RecyclerView.ViewHolder implements ImageLoader.Callbacks, CustomAsyncQueryHandler.AsyncQueryListener {

    public static final int MEDIA_TOKEN = 0;

    private CustomImage mImage;
    private TextView mDescription;
    private RecyclerView mRecyclerView;
    private Context mContext;
    private GalleryAdapter mGalleryAdapter;

    public static final String SELECTION_CURRENT_FEED = MediaContent.MediaContentEntry.COLUMN_FEED_ID + "=?";

    public DetailViewHeaderHolder(View itemView, Context context) {
        super(itemView);
        mImage = (CustomImage) itemView.findViewById(R.id.image);
        setImageViewMeasure();
        mDescription = (TextView) itemView.findViewById(R.id.description);
        mRecyclerView = (RecyclerView) itemView.findViewById(R.id.gallery);
        this.mContext = context;
    }

    private void setImageViewMeasure() {

    }

    public void bindView(Cursor cursor, String currentFeedId) {
        ImageLoader.getInstance(mContext).load(cursor.getString(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_IMAGE_URL)), true, this);
        mDescription.setText(cursor.getString(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_FEED_DESCRIPTION)));

        new CustomAsyncQueryHandler(mContext.getContentResolver()).setQueryListener(this).startQuery(
                MEDIA_TOKEN, null, DataContentProvider.CONTENT_URI_MEDIA, null, SELECTION_CURRENT_FEED, new String[]{currentFeedId}, null);
        setGallery();
    }

    private void initAdapter(Cursor cursor) {

        mGalleryAdapter = new GalleryAdapter(mContext, cursor);
        mRecyclerView.setAdapter(mGalleryAdapter);
        mRecyclerView.addItemDecoration(new GalleryDivider(mContext));
    }

    private void setGallery() {
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
    }


    @Override
    public void successfully(Bitmap bitmap) {
        mImage.setImageBitmap(bitmap);
    }

    @Override
    public void failLoad(Bitmap bitmap) {
        mImage.setImageBitmap(bitmap);
    }


    public void closeCursor() {
        mGalleryAdapter.closeCursor();
    }

    @Override
    public void onQueryComplete(int token, Object cookie, Cursor cursor) {
        initAdapter(cursor);
    }
}
